//
//  Sake.swift
//  sakeai_ver1
//
//  Created by 新山大地 on 2019/09/26.
//  Copyright © 2019 Daichi Niiyama. All rights reserved.
//

import UIKit
import os.log
import RealmSwift

class User: Object {
    // id（主キーにするプロパティ)
    @objc dynamic var id : Int = 0
    //ユーザー名（インデックス化するプロパティ）
    @objc dynamic var name : String = ""
    //記録する酒
    let sakes = List<Sake>()

    //主キーの作成
    override static func primaryKey() -> String? {
        return "id"
    }
    override static func indexedProperties() -> [String] {
        return ["name"]
    }
}


class Sake: Object {
    
    //MARK:Properties
    dynamic var sakeLabel: String = ""
    dynamic var corpLabel: String = ""
    dynamic var prefLabel: String = ""
    dynamic var photo: UIImage?
    dynamic var photos: [UIImage] = []
    dynamic var rating: Float = 0.0
    dynamic var explain: String = ""
    dynamic var amakaraParam: Float = 0.0
    dynamic var kuchiatariParam: Float = 0.0
    dynamic var kaoriParam: Float = 0.0
    
    let owner = LinkingObjects(fromType: User.self, property: "sakes")
    
    //MARK: Initialization
    convenience init?(sakeLabel: String, corpLabel: String, prefLabel: String, photo: UIImage?, rating: Float, explain: String, photos: [UIImage], amakaraParam: Float, kuchiatariParam: Float, kaoriParam: Float){
        
        self.init()
        
        // Initialization should fail if there is no name or if the rating is negative.
        if sakeLabel.isEmpty || rating < 0 {
            return nil
        }
        
        //Initialize stored properties.
        self.sakeLabel       = sakeLabel
        self.corpLabel       = corpLabel
        self.prefLabel       = prefLabel
        self.photo           = photo
        self.rating          = rating
        self.explain         = explain
        self.photos          = photos
        self.amakaraParam    = amakaraParam
        self.kuchiatariParam = kuchiatariParam
        self.kaoriParam      = kaoriParam
        

    }
    
}
