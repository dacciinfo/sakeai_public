//
//  SakeTableViewCell.swift
//  sakeai_ver1
//
//  Created by 新山大地 on 2019/09/29.
//  Copyright © 2019 Daichi Niiyama. All rights reserved.
//

import UIKit
import Cosmos

class SakeTableViewCell: UITableViewCell {
    
    //MARK: Properties
    @IBOutlet weak var sakeLabel: UILabel!
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var ratingStarView: CosmosView!
    @IBOutlet weak var explainText: UITextView!
    @IBOutlet weak var corpLabel: UILabel!
    @IBOutlet weak var prefLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
