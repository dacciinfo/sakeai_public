//
//  UIImage.swift
//  sakeai_ver1
//
//  Created by 新山大地 on 2019/11/09.
//  Copyright © 2019 Daichi Niiyama. All rights reserved.
//
import Foundation
import UIKit

extension UIImage {
    // resize image
//    func reSizeImage(reSize:CGSize)->UIImage {
//        //UIGraphicsBeginImageContext(reSize);
//        UIGraphicsBeginImageContextWithOptions(reSize,false,UIScreen.main.scale);
//        self.draw(in: CGRect(x: 0, y: 0, width: reSize.width, height: reSize.height));
//        let reSizeImage:UIImage! = UIGraphicsGetImageFromCurrentImageContext();
//        UIGraphicsEndImageContext();
//        return reSizeImage;
//    }
    func resized(toWidth width: CGFloat) -> UIImage? {
        let canvasSize = CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    
    func trimmingImage(_ image: UIImage, trimmingArea: CGRect) -> UIImage {
        let imgRef = image.cgImage?.cropping(to: trimmingArea)
        let trimImage = UIImage(cgImage: imgRef!, scale: image.scale, orientation: image.imageOrientation)
        return trimImage
    }

}
