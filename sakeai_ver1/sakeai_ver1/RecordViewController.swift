//
//  RecordViewController.swift
//  sakeai_ver1
//
//  Created by 新山大地 on 2019/09/29.
//  Copyright © 2019 Daichi Niiyama. All rights reserved.
//

import UIKit

class RecordViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    var tableView: UITableView!
    
    let data = ["銘柄を選ぶ"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //テーブルのインスタンス
        tableView = UITableView()
        //テーブルサイズを画面いっぱいに
        tableView.frame = view.frame

        //デリゲート
        tableView.delegate = self
        tableView.dataSource = self
        
        //セルをテーブルに紐付け
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        
        //データのないセルを表示しないようにするハック
        tableView.tableFooterView = UIView(frame: .zero)
        
        //テーブル表示
        view.addSubview(tableView)
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.singleLine
        tableView.separatorColor = UIColor.gray
    }
    
    //テーブル内のセクション数を決める
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    //セルの高さ
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    //セルのインスタンスを生成する
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        cell.textLabel?.text = data[0]
        cell.accessoryType = .disclosureIndicator
        //cell.accessoryView = UISwitch() // スィッチ

        return cell
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
