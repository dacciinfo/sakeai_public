//
//  ListSakeSelectViewController.swift
//  sakeai_ver1
//
//  Created by 新山大地 on 2019/09/30.
//  Copyright © 2019 Daichi Niiyama. All rights reserved.
//

import UIKit

class ListSakeSelectViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating {
    
    @IBOutlet weak var tableView: UITableView!
    let Sakes:[String] = ["特別純米酒 作田", "獺祭 純米大吟醸 磨き二割三分", "山間 17号","中取り 楯野川 純米大吟醸"]
    var searchResults:[String] = []
    var searchController = UISearchController()
    var result : String?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //let barHeight: CGFloat = UIApplication.shared.statusBarFrame.size.height
        //tableView = UITableView(frame: CGRect(x: 0, y: barHeight, width: self.view.frame.width, height: self.view.frame.height))
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.dataSource = self
        tableView.delegate = self
        self.view.addSubview(tableView)

        searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.searchBar.sizeToFit()
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.hidesNavigationBarDuringPresentation = false

        tableView.tableHeaderView = searchController.searchBar
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive {
            return searchResults.count
        } else {
            return Sakes.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        if searchController.isActive{
            cell.textLabel!.text = "\(searchResults[indexPath.row])"
        } else {
            cell.textLabel!.text = "\(Sakes[indexPath.row])"
        }
        
        return cell
    }
    
    //文字が入力される度に呼ばれる
    func updateSearchResults(for searchController: UISearchController) {
        self.searchResults = Sakes.filter{
            $0.lowercased().contains(searchController.searchBar.text!.lowercased())
        }
        self.tableView.reloadData()
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//           // セルの選択を解除
//           tableView.deselectRow(at: indexPath, animated: true)
//           // 別の画面に遷移
//           self.present(newSakeAddViewController(), animated: true, completion: nil)
//    }
    
    //各indexPathのcellがタップされた際に呼び出される。必須。
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        print("didSelectRowAt")
        tableView.deselectRow(at: indexPath, animated: false)
        segueTonewSakeAddViewController()
    }
    
    
    //各indexPathのcellがハイライトされた際に呼ばれる
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        print("didHightlightRowAt" )
        print(Sakes[indexPath.row])
        result = Sakes[indexPath.row]
    }
    
    func segueTonewSakeAddViewController() {
        self.performSegue(withIdentifier: "TonewSakeAddViewController", sender: nil)
    }
    
    
    override func prepare(for segue : UIStoryboardSegue, sender: Any?){
        if segue.identifier == "TonewSakeAddViewController" {
            print("_____________________deadLine________________________")
            let newSakeAddViewController = segue.destination as! newSakeAddViewController
            newSakeAddViewController.received = result
        }
    }
    

}
