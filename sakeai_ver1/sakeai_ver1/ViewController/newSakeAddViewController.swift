//
//  newSakeAddViewController.swift
//  sakeai_ver1
//
//  Created by 新山大地 on 2019/09/18.
//  Copyright © 2019 Daichi Niiyama. All rights reserved.
//

import UIKit
import os.log
import Cosmos
import DKImagePickerController
import RealmSwift

@IBDesignable
class newSakeAddViewController: UIViewController, UITextViewDelegate, UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    @IBOutlet weak var photo1ImageView: UIImageView!
    @IBOutlet weak var photo2ImageView: UIImageView!
    @IBOutlet weak var photo3ImageView: UIImageView!
    @IBOutlet weak var photo4ImageView: UIImageView!
    @IBOutlet weak var photo5ImageView: UIImageView!
    @IBOutlet weak var explainField: UITextView!
    @IBOutlet weak var newSakeLabel: UINavigationItem!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var ratingStarView: CosmosView!
    @IBOutlet weak var amakaraSlider: UISlider!
    @IBOutlet weak var kuchiatariSlider: UISlider!
    @IBOutlet weak var kaoriSlider: UISlider!
    

    var sake: Sake?
    var images: [UIImage] = []
    var received : String?
    var pickerController = DKImagePickerController()
    var amakaraParam : Float = 0.0
    var kuchiatariParam : Float = 0.0
    var kaoriParam : Float = 0.0
    private var selectedImageNo: Int = 0
    private var removeIndexNum: Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        newSakeLabel.title = received

        ratingStarView.settings.updateOnTouch = true
        ratingStarView.settings.fillMode = .half
        ratingStarView.settings.starSize = 44
        ratingStarView.settings.starMargin = 10

        pickerController.maxSelectableCount = 5
        pickerController.allowSwipeToSelect = false
        pickerController.showsCancelButton = true
        pickerController.containsGPSInMetadata = true

        //imagesの初期設定（特定のファイルもしくはnilで埋める）
        self.images = [ UIImage(named: "Camera")!,
                        UIImage(),
                        UIImage(),
                        UIImage(),
                        UIImage()]
        
        //編集するとき、前の情報を持ってくる。
        if let sake = sake {
            images = sake.photos
            print("imagesの数\(images.count)")
            //for文などでループimagesの要素が5個になるまで。5個埋まったらループを抜ける。
            let imgcount = images.count
            for i in imgcount ..< 5 {
                print("iの数\(i)")
                print("images\(images)")
                images.append(UIImage())
            }

            newSakeLabel.title = sake.sakeLabel
            explainField.text = sake.explain
            ratingStarView.rating = Double(sake.rating)
            amakaraSlider.value = sake.amakaraParam
            kuchiatariSlider.value = sake.kuchiatariParam
            kaoriSlider.value = sake.kaoriParam
            removeIndexNum = imgcount
        }
        
        loadImage() //imagesの画像表示
        setTextView()
        setSlider()
        
    }
    
    @IBAction func amakaraSlider(_ sender: UISlider) {
        amakaraSlider.value = sender.value
    }
    
    @IBAction func kuchiatariSlider(_ sender: UISlider) {
        kuchiatariSlider.value = sender.value
    }
    
    @IBAction func kaoriSlider(_ sender: UISlider) {
        kaoriSlider.value = sender.value
    }
    
    
    //viewが表示される直前に呼び出される
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureObserver()
        
    }
    //viewが表示されなくなる直前に呼び出される
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        removeObserver()
    }
    
    /// Notification発行
    func configureObserver() {
        let notification = NotificationCenter.default
        notification.addObserver(self, selector: #selector(keyboardWillShow(_:)),
                                 name: UIResponder.keyboardWillShowNotification, object: nil)
        notification.addObserver(self, selector: #selector(keyboardWillHide(_:)),
                                 name: UIResponder.keyboardWillHideNotification, object: nil)
        print("Notificationを発行")
    }
    //notificationを削除
    func removeObserver() {
        let notification = NotificationCenter.default
        notification.removeObserver(self)
    }

    //キーボードが現れた時に、画面全体をずらす
    @objc func keyboardWillShow(_ notification: Notification?) {
        guard let rect = (notification?.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue,
            let duration = notification?.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? TimeInterval else { return }
        UIView.animate(withDuration: duration){
            let transform = CGAffineTransform(translationX: 0, y: -(rect.size.height))
            self.view.transform = transform
        }
        print("keyboardWillShowを実行")
    }
    
    //キーボードが降りたら画面を戻す
    @objc func keyboardWillHide(_ notification: Notification){
        guard let duration = notification.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? TimeInterval else { return }
        UIView.animate(withDuration: duration){
            self.view.transform = CGAffineTransform.identity
        }
        print("keyboardWillHideを実行")
    }


    private func loadImage() {
        self.photo1ImageView.layer.borderColor = UIColor(code: "#E3E3E3").cgColor
        self.photo1ImageView.layer.borderWidth = 2
        photo1ImageView.image = self.images[0]
        photo2ImageView.image = self.images[1]
        photo3ImageView.image = self.images[2]
        photo4ImageView.image = self.images[3]
        photo5ImageView.image = self.images[4]
    }
    
    
    private func setTextView() {
        explainField.textColor = UIColor.black
        explainField.backgroundColor = UIColor.white
        explainField.layer.borderColor = UIColor.black.cgColor
        explainField.layer.borderWidth = 1.0
        explainField.layer.cornerRadius = 5
        explainField.layer.masksToBounds = true
    }
    
    private func setSlider() {
        //sotryboardで設定
    }

    //MARK: Action
    //画像５枚あるうちどこをタップしても画像選択できるよう設定
    @IBAction func selectImage(_ sender: UITapGestureRecognizer) {
        pickerController.didSelectAssets = { [unowned self] (assets: [DKAsset]) in
            self.images = [ UIImage(named: "Camera")!,UIImage(),UIImage(),UIImage(),UIImage()]
            self.removeIndexNum = assets.count
            if assets.count > 0 {
                for i in 0 ... assets.count-1 {
                    assets[i].fetchFullScreenImage(completeBlock: { (image, info) in
                        let resizeImage = image!.resized(toWidth: 320)
                        self.images[i] = resizeImage!
                        self.loadImage()
                    })
                }
            }
            else if assets.count == 0 {
                self.images = [ UIImage(named: "Camera")!,UIImage(),UIImage(),UIImage(),UIImage()]
                self.loadImage()
            }
        }
        self.present(pickerController, animated: true) {}
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //Configure the destination view controller only when the save button is pressed.
        guard let button = sender as? UIBarButtonItem, button === saveButton else {
            os_log("The save button was not pressed, cancelling", log: OSLog.default, type: .debug)
            return
        }
        let sakeLabel = newSakeLabel.title ?? ""
        let photo1 = photo1ImageView.image
        let rating = ratingStarView.rating
        let explain = explainField.text
        let corpLabel = "〇〇株式会社"
        let prefLabel = "新潟県"
        
        if images[4] == UIImage() {
            for _ in removeIndexNum ... 4 {
                images.removeLast()
            }
        }
        
        if photo1 == UIImage(named: "Camera"){
            photo1ImageView.image = UIImage(named: "defaultPhoto")
            let photo1 = photo1ImageView.image
            var sake = Sake()
            sake.sakeLabel = sakeLabel
            sake.corpLabel = corpLabel
            sake.photo = photo1
            sake.rating = Float(rating)
            sake.explain = explain!
            sake.photos = images
            sake.amakaraParam = amakaraSlider.value
            sake.kuchiatariParam = kuchiatariSlider.value
            sake.kaoriParam = kaoriSlider.value
            sake = Sake(sakeLabel: sakeLabel, corpLabel: corpLabel, prefLabel: prefLabel, photo: photo1, rating: Float(rating), explain: explain!, photos: images, amakaraParam: amakaraSlider.value, kuchiatariParam: kuchiatariSlider.value, kaoriParam: kaoriSlider.value)!
            
            let realm = try! Realm()
            try! realm.write {
                realm.add(sake)
                print("以下Realm")
                print(Realm.Configuration.defaultConfiguration.fileURL!)
            }

        } else {
            sake = Sake(sakeLabel: sakeLabel, corpLabel: corpLabel, prefLabel: prefLabel, photo: images[0], rating: Float(rating), explain: explain!, photos: images, amakaraParam: amakaraSlider.value, kuchiatariParam: kuchiatariSlider.value, kaoriParam: kaoriSlider.value)
            let realm = try! Realm()
            try! realm.write {
                realm.add(sake!)
                print("以下Realm")
                print(Realm.Configuration.defaultConfiguration.fileURL!)
            }
        }
        
    }

    
}

