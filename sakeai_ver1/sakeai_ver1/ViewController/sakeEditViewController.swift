//
//  sakeEditViewController.swift
//  sakeai_ver1
//
//  Created by 新山大地 on 2019/10/01.
//  Copyright © 2019 Daichi Niiyama. All rights reserved.
//

import UIKit
import Cosmos

class sakeEditViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var sakeEditTitle: UINavigationItem!
    @IBOutlet weak var ratingStarView: CosmosView!
    @IBOutlet weak var explainField: UITextView!
    @IBOutlet weak var amakaraSlider: UISlider!
    @IBOutlet weak var kuchiatariSlider: UISlider!
    @IBOutlet weak var kaoriSlider: UISlider!
    
    var sake: Sake?
    
    var sakeLabel: String = ""
    var corpLabel: String = ""
    var prefLabel: String = ""
    var photo: UIImage?
    var photos: [UIImage] = []
    var resizePhotos: [UIImage] = []
    var rating: Float = 0.0
    var explain: String = ""
    var amakaraParam: Float = 0.0
    var kuchiatariParam: Float = 0.0
    var kaoriParam: Float = 0.0
    var tapCount = 0
    
    //ScrollScreenの高さ
    var scrollScreenHeight: CGFloat!
    //ScrollScreenの幅
    var scrollScreenWidth: CGFloat!
    //Totalのページ数
    var pageNum:Int!
    var imageWidth:CGFloat!
    var imageHeight:CGFloat!
    var screenSize:CGRect!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        print("sake:\(String(describing: sake))")
        sakeEditTitle.title = sake?.sakeLabel
        print("photos:\(sake!.photos)")
        photos = sake!.photos
        photo = sake!.photo
        ratingStarView.rating = Double(sake!.rating)
        explainField.text = sake?.explain
        amakaraSlider.value = sake!.amakaraParam
        kuchiatariSlider.value = sake!.kuchiatariParam
        kaoriSlider.value = sake!.kaoriParam

        screenSize = UIScreen.main.bounds
        //ページスクロールとするためにページ幅を合わせる
        scrollScreenWidth = screenSize.width
        //写真を正方形にトリミングして基準となる大きさをつくる
        print("photo:\(String(describing: photo))")
        let resizePhoto:UIImage = photo!.trimmingImage(photo!, trimmingArea: CGRect(x: 0, y: 0, width: photo!.size.width, height: photo!.size.width))
//        let imageTop: UIImage = UIImage(named: img[0])!
        pageNum = photos.count
        imageWidth = resizePhoto.size.width
        imageHeight = resizePhoto.size.height
//        imageWidth = photo!.size.width
//        imageHeight = photo!.size.height
        //取得画像の縦横比率とスマホ画面の横サイズから
        scrollScreenHeight = scrollScreenWidth * imageHeight/imageWidth
        
        print("imageWidth:\(String(describing: imageWidth))")
        print("imageHeight:\(String(describing: imageHeight))")
        print("scrollScreenWidth:\(String(describing: scrollScreenWidth))")
        print("scrollScreenHeight:\(String(describing: scrollScreenHeight))")
        print("photos\(photos)")
        
        for i in 0 ..< pageNum {
            //UIImageViewのインスタンス
//            let image:UIImage = UIImage(named:img[i])!
            let image:UIImage = photos[i]
            print("\(i)回目")
            print("photos[i]:\(photos[i])")
            print("image:\(image)")
            let resizeImage:UIImage = photos[i].trimmingImage(photos[i], trimmingArea: CGRect(x: 0, y: 0, width: photo!.size.width, height: photo!.size.width))
            print("image:\(image)")
            print("imageresizeImage:\(resizeImage)")
            let imageView = UIImageView(image:resizeImage)
            var rect: CGRect = imageView.frame

            rect.size.height = scrollScreenHeight
            rect.size.width = scrollScreenWidth
            imageView.frame = rect
            imageView.tag = i + 1
            print("imageView:\(imageView)")
            //UIScrollViewのインスタンスに画像を貼り付ける
            self.scrollView.addSubview(imageView)
            print("scrollView:\(String(describing: scrollView))")
        }
        setupScrollImages()
        setTextView()
        setRatingStar()
        setSlider()
    }
    
    private func setTextView() {
        explainField.textColor = UIColor.black
        explainField.backgroundColor = UIColor.white
        explainField.layer.borderColor = UIColor.black.cgColor
        explainField.layer.borderWidth = 1.0
        explainField.layer.cornerRadius = 5
        explainField.layer.masksToBounds = true
        explainField.isEditable = false
//        explainField.text = explain
    }
    
    private func setRatingStar() {
        ratingStarView.settings.updateOnTouch = false
        ratingStarView.settings.fillMode = .half
        ratingStarView.settings.starSize = 44
        ratingStarView.settings.starMargin = 10
//        ratingStarView.rating = Double(rating)
    }
    
    private func setSlider() {
//        amakaraSlider.value = amakaraParam
//        kuchiatariSlider.value = kuchiatariParam
//        kaoriSlider.value = kaoriParam
        amakaraSlider.isUserInteractionEnabled = false
        kuchiatariSlider.isUserInteractionEnabled = false
        kaoriSlider.isUserInteractionEnabled = false
    }

    func setupScrollImages() {
        //ダミー画像
//        let imageDummy:UIImage = photo!
        let imageDummy: UIImage = UIImage(named: "defaultPhoto")!
        var imgView = UIImageView(image: imageDummy)
        let subviews:Array = scrollView.subviews
        print("subviews:\(String(describing: subviews))")
        //描画開始の x,y 位置
        var px:CGFloat = 0.0
        let py:CGFloat = 0.0
        
        for i in 2 ..< subviews.count {
            imgView = subviews[i] as! UIImageView
            print("imgView.tag:\(imgView.tag)")
            if(imgView.isKind(of: UIImageView.self) && imgView.tag > 0){

                var viewFrame:CGRect = imgView.frame
                viewFrame.origin = CGPoint(x: px, y: py)
                imgView.frame = viewFrame

                px += (scrollScreenWidth)
                print(i)
                print("imgView\(imgView)")
                print()
            }
        }
            //UIScrollViewのコンテンツサイズを画像のtotalサイズに合わせる
            let nWidth:CGFloat = scrollScreenWidth * CGFloat(pageNum)
            scrollView.contentSize = CGSize(width: nWidth, height: scrollScreenHeight)
    }
        
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "tonewSakeAddViewController" {
            let newSakeAddViewController = segue.destination as! newSakeAddViewController
            newSakeAddViewController.sake = sake

        }
        else if segue.identifier == "AddItem" {
            print("Adding new meal.")
        }
    }
    
    
    @IBAction func enlargeImage(_ sender: UITapGestureRecognizer) {
//        //tapカウントが偶数のとき写真を拡大
//        if tapCount % 2 == 0 {
//            let imageView = UIImageView(image:photo)
//            // スクリーンの縦横サイズを取得
//            let screenWidth:CGFloat = view.frame.size.width
//            let screenHeight:CGFloat = view.frame.size.height
//            // 画像の縦横サイズを取得
//            let imgWidth:CGFloat = photo!.size.width
//            let imgHeight:CGFloat = photo!.size.height
//            // 画像サイズをスクリーン幅に合わせる
//            let scale:CGFloat = screenWidth / imgWidth
//            let rect:CGRect =
//                CGRect(x:0, y:0, width:imgWidth*scale, height:imgHeight*scale)
//            // ImageView frame をCGRectで作った矩形に合わせる
//            imageView.frame = rect;
//            // 画像の中心を画面の中心に設定
//            imageView.center = CGPoint(x:screenWidth/2, y:screenHeight/2)
//            // UIImageViewのインスタンスをビューに追加
//            self.view.addSubview(imageView)
//            tapCount += 1
//        }
    }
    
}
