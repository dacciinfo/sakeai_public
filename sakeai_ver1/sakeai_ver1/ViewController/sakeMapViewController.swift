//
//  sakeMapViewController.swift
//  sakeai_ver1
//
//  Created by 新山大地 on 2019/10/22.
//  Copyright © 2019 Daichi Niiyama. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON
 
class sakeMapViewController: UIViewController {
    var googleMap: GMSMapView?
    var locationManager = CLLocationManager()
    //画面中心の緯度経度
    var centerMapCoordinate:CLLocationCoordinate2D!
    let marker = GMSMarker()
    var markers : [GMSMarker] = []
    
    @IBOutlet weak var locationButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var listButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        googleMap = GMSMapView()
        setupMapView()        
        setupButton()
        self.googleMap?.delegate = self
    }
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //現在地に戻るボタンアクション
    @IBAction func locationTappedButton(_ sender: UIButton) {
        guard let lat = self.googleMap?.myLocation?.coordinate.latitude,
            let lng = self.googleMap?.myLocation?.coordinate.longitude else { return }
        let camera = GMSCameraPosition.camera(withLatitude: lat ,longitude: lng , zoom: 15.0)
        self.googleMap?.animate(to: camera)
    }
    
    @IBAction func searchTappedButton(_ sender: UIButton) {
        print("searchTappedButton")
        googleMap?.clear()
    }
    
    //3つのボタンを設置
    func setupButton(){
        locationButton.layer.cornerRadius = 21
        locationButton.layer.shadowOpacity = 0.3
        locationButton.layer.shadowRadius = 3
        locationButton.layer.shadowOffset = CGSize(width: 3, height: 3)
        locationButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10);
        searchButton.layer.cornerRadius = 21
        searchButton.layer.shadowOpacity = 0.3
        searchButton.layer.shadowRadius = 3
        searchButton.layer.shadowOffset = CGSize(width: 3, height: 3)
        searchButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10);
        listButton.layer.cornerRadius = 21
        listButton.layer.shadowOpacity = 0.3
        listButton.layer.shadowRadius = 3
        listButton.layer.shadowOffset = CGSize(width: 3, height: 3)
        listButton.imageEdgeInsets = UIEdgeInsets(top: 13, left: 10, bottom: 13, right: 10);
        self.view.addSubview(locationButton)
        self.view.addSubview(searchButton)
        self.view.addSubview(listButton)
    }
    
    //場所のマッピング
    func getPlaces(coordinate: CLLocationCoordinate2D){
        //周辺の酒屋を取得するurlを定義
        let requestURLString = "https://map.yahooapis.jp/search/local/V1/localSearch?appid=dj00aiZpPXhQU0hzS1JzMzVpbiZzPWNvbnN1bWVyc2VjcmV0Jng9OTg-&query=%E9%85%92%E5%B1%8B&dist=3&output=json&sort=dist" + "&lat=" + String(coordinate.latitude) + "&lon=" + String(coordinate.longitude)
        //json取得、marker設置
        Alamofire.request(requestURLString).responseJSON{ response in
            if let jsonObject = response.result.value {
                let json = JSON(jsonObject)
                let features = json["Feature"]
                
                for (_ , subJson):(String, JSON) in features {
                    let coordinate = subJson["Geometry"]["Coordinates"].stringValue.split(separator: ",")
                    let marker = GMSMarker()
                    marker.position = CLLocationCoordinate2D(latitude:Double(coordinate[1])!, longitude:Double(coordinate[0])!)
                    marker.title = subJson["Name"].stringValue
                    marker.snippet = subJson["Property"]["Address"].stringValue
                    self.googleMap?.selectedMarker = marker
                    marker.map = self.googleMap
                    self.markers.append(marker)
                }
            }
        }
    }
 
}

extension sakeMapViewController: GMSMapViewDelegate{
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if (status == CLAuthorizationStatus.authorizedWhenInUse) {
            googleMap?.isMyLocationEnabled = true
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let newLocation = locations.last
        googleMap?.camera = GMSCameraPosition.camera(withTarget: newLocation!.coordinate, zoom: 15.0)
        self.view = self.googleMap
    }
    
    //camreraが変更されたらそれに合わせてマッピング
    func mapView(_ mapView: GMSMapView, idleAt idleAtCameraPosition: GMSCameraPosition) {
        let latitude = googleMap?.camera.target.latitude
        let longitude = googleMap?.camera.target.longitude
        centerMapCoordinate = CLLocationCoordinate2D(latitude: latitude!, longitude: longitude!)
        
//        //clear()、deleteall()、nilなどしたが消えないので後回し
//        googleMap?.clear()
        self.getPlaces(coordinate: centerMapCoordinate)
        // 配列をループして取り出し
        for(marker) in markers {
            // 地図の表示領域内に含まれるか
            if(googleMap?.projection.contains(marker.position) ?? false) {
                // 含まれる場合
                marker.map = self.googleMap
            } else {
                // 含まれない場合
                marker.map = nil
            }
        }
    }
    
}

extension sakeMapViewController: CLLocationManagerDelegate{
    func setupMapView(){
        let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: 22.300000, longitude: 70.783300, zoom: 15.0)
        googleMap = GMSMapView.map(withFrame: self.view.frame, camera: camera)
        googleMap?.camera = camera

        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
        locationManager.distanceFilter = 500
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
        self.view = googleMap
    }
    
}
