//
//  MainViewController.swift
//  sakeai_ver1
//
//  Created by 新山大地 on 2019/10/23.
//  Copyright © 2019 Daichi Niiyama. All rights reserved.
//

import UIKit
import SwiftUI

@IBDesignable
class MainViewController: UIViewController{

    var searchBar: UISearchBar!
    
    @IBOutlet weak var detailSakeSearchView1: UIView!
    @IBOutlet weak var detailSakeSearchView2: UIView!
    @IBOutlet weak var detailSakeSearchView3: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupSearchBar()
        setupDetailSakeSearchView()
    }
    
    func setupSearchBar(){
        if let navigationBarFrame = navigationController?.navigationBar.bounds {
            let searchBar: UISearchBar = UISearchBar(frame: navigationBarFrame)
            searchBar.delegate = self
            searchBar.placeholder = "銘柄　蔵元　など"
            searchBar.tintColor = UIColor(code: "#c0c0c0")
            searchBar.keyboardType = UIKeyboardType.default
            navigationItem.titleView = searchBar
            navigationItem.titleView?.frame = searchBar.frame
            self.searchBar = searchBar
        }
    }
    
    func setupDetailSakeSearchView() {
        detailSakeSearchView1.layer.cornerRadius = 14
        detailSakeSearchView2.layer.cornerRadius = 14
        detailSakeSearchView3.layer.cornerRadius = 14
        detailSakeSearchView1.layer.borderColor = UIColor.lightGray.cgColor
        detailSakeSearchView2.layer.borderColor = UIColor.lightGray.cgColor
        detailSakeSearchView3.layer.borderColor = UIColor.lightGray.cgColor
        detailSakeSearchView1.layer.borderWidth = 1
        detailSakeSearchView2.layer.borderWidth = 1
        detailSakeSearchView3.layer.borderWidth = 1
    }
    
}


extension MainViewController: UISearchBarDelegate{
    // 編集が開始されたら、キャンセルボタンを有効にする
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.showsCancelButton = true
        return true
    }
    // キャンセルボタンが押されたらキャンセルボタンを無効にしてフォーカスを外す
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.resignFirstResponder()
    }
}
