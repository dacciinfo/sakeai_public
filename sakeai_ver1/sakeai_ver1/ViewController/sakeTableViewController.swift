//
//  sakeTableViewController.swift
//  sakeai_ver1
//
//  Created by 新山大地 on 2019/09/29.
//  Copyright © 2019 Daichi Niiyama. All rights reserved.
//

import UIKit
import Cosmos
import os.log
import RealmSwift

class sakeTableViewController: UITableViewController {
    //MARK: Properties
//    var sakes = [Sake]()
    let sakes = List<Sake>()
    var sakeLabel: String = ""
    var corpLabel: String = ""
    var prefLabel: String = ""
    var photo: UIImage?
    var photos: [UIImage] = []
    var rating: Float = 0.0
    var explain: String = ""
    var amakaraParam: Float = 0.0
    var kuchiatariParam: Float = 0.0
    var kaoriParam: Float = 0.0
    
    let realm = try! Realm()
    
    //MARK: Actions
    @IBAction func unwindToSakeList(sender: UIStoryboardSegue){
        if let sourceViewController = sender.source as? newSakeAddViewController,
            let sake = sourceViewController.sake{
            
            if let selectedIndexPath = tableView.indexPathForSelectedRow {
                sakes[selectedIndexPath.row] = sake
                tableView.reloadRows(at: [selectedIndexPath], with: .none)
            } else {
                let newIndexPath = IndexPath(row: sakes.count, section: 0)
                
                sakes.append(sake)
                tableView.insertRows(at: [newIndexPath], with: .automatic)
            }
//            saveSakes()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadSampleSakes()
//        if let savedSakes = loadSakes() {
//            sakes += savedSakes
//        } else {
//            loadSampleSakes()
//        }
        tableView.separatorColor = .gray
        print(sakes)
    }
    
    //MARK: Private Methods
    private func loadSampleSakes(){
        let photo1 = UIImage(named: "sake1")
        let photo2 = UIImage(named: "sake2")
        let photo3 = UIImage(named: "sake3")
//        let sake1 = Sake()
//        sake1.sakeLabel = "sakuta"
//        sake1.corpLabel = "株式会社盛田庄兵衛"
//        sake1.prefLabel = "青森県"
//        sake1.photo = photo1
//        sake1.rating = 4.5
//        sake1.explain = "aomori no sake"
//        sake1.photos = [photo1!]
//        sake1.amakaraParam = 0.4
//        sake1.kuchiatariParam = 0.2
//        sake1.kaoriParam = 0.3
        guard let sake1 = Sake(sakeLabel:"sakuta",corpLabel: "株式会社盛田庄兵衛", prefLabel: "青森県", photo: photo1, rating: 4.5, explain: "aomori no sake", photos: [photo1!], amakaraParam: 0.4, kuchiatariParam: 0.2, kaoriParam: 0.3) else {
            fatalError("Unable to instaniate sake1")
        }
        guard let sake2 = Sake(sakeLabel:"dansyu",corpLabel: "西田酒造店", prefLabel: "青森県", photo: photo2, rating: 3.2, explain: "aomori no sake", photos: [photo2!], amakaraParam: 0.4, kuchiatariParam: 0.2, kaoriParam: 0.3) else {
            fatalError("Unable to instaniate sake2")
        }
        guard let sake3 = Sake(sakeLabel:"zaku",corpLabel: "清水醸造", prefLabel: "三重県", photo: photo3, rating: 5, explain: "mie no sake", photos: [photo3!], amakaraParam: 0.4, kuchiatariParam: 0.2, kaoriParam: 0.3) else {
            fatalError("Unable to instaniate sake3")
        }
//        sakes += [sake1, sake2, sake3]
        sakes.append(sake1)
        sakes.append(sake2)
        sakes.append(sake3)
        try! realm.write {
            realm.add(sakes)
        }
        let results = realm.objects(Sake.self)
        print(results)
    }
    
//    private func saveSakes() {
//        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(sakes, toFile: Sake.ArchiveURL.path)
//        if isSuccessfulSave {
//            os_log("Meals successfully saved.", log: OSLog.default, type: .debug)
//        } else {
//            os_log("Failed to save meals...", log: OSLog.default, type: .error)
//        }
//    }
//
//    private func loadSakes() -> [Sake]?  {
//        return NSKeyedUnarchiver.unarchiveObject(withFile: Sake.ArchiveURL.path) as? [Sake]
//    }
    

}

extension sakeTableViewController{
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sakes.count
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let dialog = UIAlertController(title: "確認", message: "削除しますか？", preferredStyle: .alert)
            dialog.addAction(UIAlertAction(title: "削除", style: .destructive,
            handler: { action in
                self.sakes.remove(at: indexPath.row)
//                self.saveSakes()
                tableView.deleteRows(at: [indexPath], with: .fade)
            }))
            dialog.addAction(UIAlertAction(title: "キャンセル", style: .cancel, handler: nil))
            self.present(dialog, animated: true, completion: nil)
            
        } else if editingStyle == .insert {
            
        }
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "SakeTableViewCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? SakeTableViewCell else {
            fatalError("The dequeued cell is not an instance of SakeTableViewCell.")
        }
        // Fetches the appropriate sake for the data source layout.
        let sake = sakes[indexPath.row]

        tableView.backgroundColor = .white
        cell.sakeLabel.text = sake.sakeLabel
        cell.photoImageView.image = sake.photo
        cell.ratingStarView.settings.updateOnTouch = false
        cell.ratingStarView.settings.fillMode      = .precise
        cell.ratingStarView.settings.starSize = 30
        cell.ratingStarView.rating = Double(sake.rating)
        cell.explainText.text = sake.explain
        cell.explainText.textColor = .black
        cell.explainText.backgroundColor = UIColor.clear
        cell.explainText.isUserInteractionEnabled = true
        cell.explainText.isEditable = false
        cell.corpLabel.text = sake.corpLabel
        cell.prefLabel.text = sake.prefLabel

        return cell
    }
//    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        sakeLabel           = sakes[indexPath.row].sakeLabel
//        corpLabel           = sakes[indexPath.row].corpLabel
//        prefLabel           = sakes[indexPath.row].prefLabel
//        photo               = sakes[indexPath.row].photo
//        photos              = sakes[indexPath.row].photos
//        rating              = sakes[indexPath.row].rating
//        explain             = sakes[indexPath.row].explain
//        amakaraParam        = sakes[indexPath.row].amakaraParam
//        kuchiatariParam     = sakes[indexPath.row].kuchiatariParam
//        kaoriParam          = sakes[indexPath.row].kaoriParam
//        performSegue(withIdentifier: "ToSakeEditViewController", sender: nil)
//        print("\(indexPath.row)番目の行が選択されました。")
//    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if (segue.identifier == "ToSakeEditViewController") {
//            let sakeEditViewController = segue.destination as! sakeEditViewController
//            sakeEditViewController.sakeLabel = sakeLabel
//            sakeEditViewController.corpLabel = corpLabel
//            sakeEditViewController.prefLabel = prefLabel
//            sakeEditViewController.photo = photo
//            sakeEditViewController.photos = photos
//            sakeEditViewController.rating = rating
//            sakeEditViewController.explain = explain
//            sakeEditViewController.amakaraParam = amakaraParam
//            sakeEditViewController.kuchiatariParam = kuchiatariParam
//            sakeEditViewController.kaoriParam = kaoriParam
//            }
//        }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ToSakeEditViewController" {
            let sakeEditViewController = segue.destination as! sakeEditViewController
            if let selectedSakeCell = sender as? SakeTableViewCell {
                let indexPath = tableView.indexPath(for: selectedSakeCell)!
                let selectedSake = sakes[indexPath.row]
                sakeEditViewController.sake = selectedSake
            }
        }
        else if segue.identifier == "AddItem" {
            print("Adding new meal.")
        }
    }
    
    
}
