//
//  SakeEditModalViewController.swift
//  sakeai_ver1
//
//  Created by 新山大地 on 2019/11/10.
//  Copyright © 2019 Daichi Niiyama. All rights reserved.
//

import UIKit

class SakeEditModalViewController: UIViewController {
    
    var tapCount = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func hideNavigationBar(_ sender: UITapGestureRecognizer) {
        if tapCount % 2 == 0 {
            navigationController?.setNavigationBarHidden(true, animated: false)
            tapCount += 1
        }else {
            navigationController?.setNavigationBarHidden(false, animated: false)
            tapCount += 1
        }
        
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
